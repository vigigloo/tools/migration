{{/*
Generate unique fullname for each job
*/}}
{{- define "job.fullname" -}}
{{- printf "%s-%s" (include "common.fullname" .values) .jobValues.name | trunc 63 | trimSuffix "-" }}
{{- end }}
